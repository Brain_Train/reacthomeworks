import React from 'react';

import Combined from './componrnts/combined/Combained';
import Form from './componrnts/Form/Form'
import Funny from './componrnts/funny/Funny';
import Baloon from './componrnts/movingBaloon/Baloon';
import Card from './componrnts/card/Card'
import './Form.css';

function App() {
  return (
    <div className="Form">
      <Form />
      <Baloon />
      <Funny />
      <Combined />
      <div className="cards">
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
      </div>
    </div>
  );
}

export default App;
