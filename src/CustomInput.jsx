import React from 'react'


export const CustomInput = ({ type, placeholder }) => {
   return (
      <div>
         <input 
            type={type}
            placeholder={placeholder} 
         />
      </div>
   )
}

// class CustomInput extends Component {
//    render() {
//       return (
//          <div>
//             <input type="text" />
//          </div>
//       )
//    }
// }

// export default CustomInput