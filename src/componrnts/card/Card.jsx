import React from 'react';

import { Button } from "../button/Button";
import img from './img/img.PNG';
import './card.style.css';

const Card = () => {
   return (
      <div 
         className="card"
      >
         <img src={img} alt="#"/>
         <h4>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. A, minus.
         </h4>
         <Button text="Add"/>
      </div>
   )
}

export default Card
