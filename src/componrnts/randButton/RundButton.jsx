import React, { Component } from 'react';

import './rundButtom.css';

export class RandButton extends Component {
   constructor(props) {
      super(props);
      this.state = {
         divColor: "white",
         buttonColor: "white"
      };
   }

   handleButtonClick = () => {
   this.setState({
      divColor: "red",
      buttonColor: "red"
   })
   }

   render() {
      return (
      <React.Fragment>
         <div 
            className="test" 
            style={{background: this.state.divColor}}
         >
            Area of changing color
         </div>
         <button 
            className="randButt"
            style={{background: this.state.buttonColor}}
            onClick={this.handleButtonClick}
         >
            Click me at once!
         </button>
      </React.Fragment>
      );
   }
}
