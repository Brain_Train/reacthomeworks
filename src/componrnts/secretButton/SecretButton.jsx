import React from 'react';

import './secret.style.css';

export const SecretButton = ({ text }) => {
   const handleClick = () => {
      alert('You spent 5 seconds of your life on this, are you happy?')
   }
   return (
      <button 
         onClick={handleClick}
         className="secretButton"
      >
         {text}
      </button>
   )
}
