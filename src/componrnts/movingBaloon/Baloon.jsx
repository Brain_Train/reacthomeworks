import React, { Component } from 'react';

import './baloon.style.css';

class Baloon extends Component {
   constructor(props) {
      super(props);
      this.animation = React.createRef();
   }
   handleClick() {
      const anim = this.animation.current;
      anim.classList.toggle('move')
   }
   render() {
      return (
         <div 
            ref={this.animation} 
            className="start"
         >
            <div 
               id="move" 
               className="baloon"
               onClick={() => this.handleClick()}
            >
               <p>Attention don't press here :c </p>
            </div>
         </div>
      );
   }
}

export default Baloon
