import React from 'react';

import './button.style.css';

export const Button = ({ text }) => {
   const handleClick = () => {
      console.log('click');
   }
   return (
      <button 
         onClick={handleClick}
         className="button"
      >
         {text}
      </button>
   )
}
