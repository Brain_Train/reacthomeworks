import React, { Component } from 'react';

import { Button } from "../button/Button";
import { CustomInput } from "../../CustomInput";
import { SecretButton } from '../secretButton/SecretButton';
import { RandButton } from '../randButton/RundButton';
import './combained.style.css';

class Combined extends Component {
   render() {
      return (
         <div 
            className="final"
         >
            <p>Combined</p>
            <SecretButton text="oh no-no-no"/>
            <CustomInput placeholder="Something" type="text"/>
            <Button text="press it"/>
            <RandButton />
         </div>
      )
   }
}

export default Combined
